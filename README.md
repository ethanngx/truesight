# TrueSight
TrueSight is a web-site based application which provides more information, is user-driven, and allows cross-platform comparison. In TrueSight, it has more information about the applications, includes the names, organizations, platforms, versions of it can be installed to, an external link to the respective stores, and the price. Also, users with registration can add new applications in TrueSight.

## About
1. You are able to view, sort, and filter all the applications in the repository without logging in.
2. There is a consistent search bar on each page for string search and sorting. Users can get the application information according to the application name they are interested in and the order they expect.
3. Welcome to join TrueSight by creating an account. You can simply register by creating the username and setting the password you like. 
4. All registered users have the ability to post their new applications in TrueSight. Join and help us to make truesight better.

## Extra Service For Registered User:
The users with registration can post their new applications in TrueSight.
1. Firstly, you need to log in to your account
1. In order to add new application to the repository, you are supposed to submit a request form first which includes an external link to the platform-specific application page.
2. Then, the admin will verify it. You will receive the feedback on whether the request approves or rejects in 48 hours.

The information of the application contains the name of the application, a description, the organization, the platforms, the versions it can be installed to, an external link to the respective stores, and the price.

## Notice
- Administrators can access an outstanding request page to check all submitted application requests and respond or take action on them.

