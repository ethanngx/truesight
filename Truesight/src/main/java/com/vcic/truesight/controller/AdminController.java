package com.vcic.truesight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vcic.truesight.dao.ApplicationDAO;
import com.vcic.truesight.model.Application;
import com.vcic.truesight.model.Request;

@Controller
public class AdminController {
	@Autowired
	private ApplicationDAO appDAO;

	@RequestMapping(value = "/request/{request_id}", method = RequestMethod.GET)
	public String appPage(@PathVariable("request_id") int request_id, Model model) {
		Request req = appDAO.findRequest(request_id);
		model.addAttribute("req", req);
		return "requestPage";
	}

	@RequestMapping(value = "/acceptRequest", method = RequestMethod.GET)
	public String acceptRequestPage(@RequestParam("rejectId") int request_id, @RequestParam("name") String name,
			@RequestParam("description") String description, @RequestParam("organization") String organization,
			@RequestParam("platform") String platform, @RequestParam("version") String version,
			@RequestParam("link") String link, @RequestParam("price") double price, @RequestParam("img") String img) {
		List<Application> appList = appDAO.getAllApp();
		int app_id;
		if (appList.size() == 0) {
			app_id = 1;
		} else {
			app_id = appList.get(appList.size() - 1).getApp_id() + 1;
		}
		appDAO.deleteRequest(request_id);
		appDAO.addApplication(app_id, name, description, organization, img, price, platform, version, link);
		return "addSuccessPage";
	}

	@RequestMapping(value = "/rejectRequest", method = RequestMethod.GET)
	public String rejectRequestPage(@RequestParam("rejectId") int request_id) {
		appDAO.deleteRequest(request_id);
		return "rejectRequestPage";
	}

	@RequestMapping(value = "/addApp", method = RequestMethod.GET)
	public String appUpdatePage(Model model) {
		return "addAppPage";
	}

	@RequestMapping(value = "/addSuccess", method = RequestMethod.GET)
	public String addSuccessPage(@RequestParam("name") String name, @RequestParam("description") String description,
			@RequestParam("organization") String organization, @RequestParam("platform") String platform,
			@RequestParam("version") String version, @RequestParam("link") String link,
			@RequestParam("price") double price, @RequestParam("img") String img) {
		List<Application> appList = appDAO.getAllApp();
		int app_id;
		if (appList.size() == 0) {
			app_id = 1;
		} else {
			app_id = appList.get(appList.size() - 1).getApp_id() + 1;
		}
		appDAO.addApplication(app_id, name, description, organization, img, price, platform, version, link);
		return "addSuccessPage";
	}

	@RequestMapping(value = "/deleteApp/{app_id}", method = RequestMethod.GET)
	public String deleteApp(@PathVariable("app_id") int app_id, Model model) {
		appDAO.deleteApplication(app_id);
		model.addAttribute("reqList", appDAO.getRequests());
		model.addAttribute("appList", appDAO.getAllApp());
		return "adminPage";
	}

	@RequestMapping(value = "/appUpdate/{app_id}", method = RequestMethod.GET)
	public String appUpdatePage(@PathVariable("app_id") int app_id, Model model) {
		List<Application> list = appDAO.findApp(app_id);
		model.addAttribute("list", list);
		model.addAttribute("app", list.get(0));
		return "appUpdatePage";
	}

	@RequestMapping(value = "/updateSuccess", method = RequestMethod.GET)
	public String updateSuccessPage(@RequestParam("app_id") int app_id, @RequestParam("name") String name,
			@RequestParam("description") String description, @RequestParam("organization") String organization,
			@RequestParam("platform") List<String> platforms, @RequestParam("version") List<String> versions,
			@RequestParam("link") List<String> links, @RequestParam("price") double price,
			@RequestParam("img") String img) {
		appDAO.updateApplication(app_id, name, description, organization, img, price);
		for (int i = 0; i < platforms.size(); i++) {
			appDAO.updatePlatform(app_id, platforms.get(i), versions.get(i), links.get(i));
		}
		return "updateSuccessPage";
	}

	@RequestMapping(value = "/deletePlatform/{app_id}/{platform}", method = RequestMethod.GET)
	public String deletePlatform(@PathVariable("app_id") int app_id, @PathVariable("platform") String platform,
			Model model) {
		appDAO.deletePlatform(app_id, platform);
		List<Application> list = appDAO.findApp(app_id);
		if (list.size() == 0) {
			appDAO.deleteApplication(app_id);
			model.addAttribute("reqList", appDAO.getRequests());
			model.addAttribute("appList", appDAO.getAllApp());
			return "adminPage";
		} else {
			model.addAttribute("list", list);
			model.addAttribute("app", list.get(0));
			return "appUpdatePage";
		}
	}
}
