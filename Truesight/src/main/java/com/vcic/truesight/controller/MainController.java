package com.vcic.truesight.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vcic.truesight.dao.ApplicationDAO;
import com.vcic.truesight.dao.UserInfoDAO;
import com.vcic.truesight.model.Application;

@Controller
public class MainController {
	@Autowired
	private ApplicationDAO appDAO;

	@Autowired
	private UserInfoDAO userDAO;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcomePage(Model model) {
		model.addAttribute("appList", appDAO.getAllApp());
		return "welcomePage";
	}

	@RequestMapping(value = "/app/{app_id}", method = RequestMethod.GET)
	public String appPage(@PathVariable("app_id") int app_id, Model model) {
		List<Application> list = appDAO.findApp(app_id);
		model.addAttribute("list", list);
		model.addAttribute("app", list.get(0));
		return "appPage";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchPage(@RequestParam("name") String name, Model model) {
		model.addAttribute("search", name);
		model.addAttribute("appList", appDAO.findAppByName(name));
		return "searchPage";
	}

	@RequestMapping(value = "/sort", method = RequestMethod.GET)
	public String sortPage(@RequestParam("sortPrice") String sort, Model model) {
		model.addAttribute("appList", appDAO.sortAppByPrice(sort));
		return "sortPage";
	}

	@RequestMapping(value = "/filter", method = RequestMethod.GET)
	public String filterPage(@RequestParam("startFilterPrice") int startFilter,
			@RequestParam("endFilterPrice") int endFilter, Model model) {
		model.addAttribute("appList", appDAO.filterAppByPrice(startFilter, endFilter));
		return "filterPage";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
		return "loginPage";
	}

	@RequestMapping(value = "/signUpSuccess", method = RequestMethod.POST)
	public String signUpPage(@RequestParam("username") String username, @RequestParam() String password, Model model) {
		userDAO.addUserInfo(username, password);
		return "signUpSuccess";
	}

	@RequestMapping(value = "/signUp", method = RequestMethod.GET)
	public String signupPage(Model model) {
		return "signUpPage";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(Model model) {
		model.addAttribute("reqList", appDAO.getRequests());
		model.addAttribute("appList", appDAO.getAllApp());
		return "adminPage";
	}

	@RequestMapping(value = "/userInfo", method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {
		// After logging in successfully, the logged in is the principal
		return "userInfoPage";
	}

	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public String logoutSuccessfulPage(Model model) {
		return "logoutSuccessfulPage";
	}

	@RequestMapping(value = "helpPage", method = RequestMethod.GET)
	public String helpPage(Model model) {
		return "helpPage";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model, Principal principal) {

		if (principal != null) {
			model.addAttribute("message",
					"Hi " + principal.getName() + "<br> You do not have permission to access this page!");
		} else {
			model.addAttribute("message", "You do not have permission to access this page!");
		}
		return "403Page";
	}
}
