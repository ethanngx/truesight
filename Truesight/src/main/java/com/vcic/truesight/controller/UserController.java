package com.vcic.truesight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vcic.truesight.dao.ApplicationDAO;
import com.vcic.truesight.model.Request;

@Controller
public class UserController {
	@Autowired
	private ApplicationDAO appDAO;
	
	@RequestMapping(value = "/requested", method = RequestMethod.GET)
	public String requestSuccessfulPage(@RequestParam("appName") String appName, @RequestParam("organization") String organization,
			@RequestParam("platform") String platform, @RequestParam("version") String version,
			@RequestParam("link") String link, @RequestParam("price") double price,
			@RequestParam("imgLink") String imgLink, @RequestParam("description") String description, Model model) {
		List<Request> reqList = appDAO.getRequests();
		int reqId;
		if (reqList.size() == 0) {
			reqId = 1;
		} else {
			reqId =  reqList.get(reqList.size() - 1).getRequest_id() + 1;
		}
		appDAO.saveRequest(reqId, appName, organization, platform, version, link, price, imgLink, description);
		return "requestSuccessfulPage";
	}
}
