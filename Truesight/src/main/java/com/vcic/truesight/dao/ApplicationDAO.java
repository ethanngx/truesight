package com.vcic.truesight.dao;

import java.util.List;

import com.vcic.truesight.model.Application;
import com.vcic.truesight.model.Request;

public interface ApplicationDAO {
	public List<Application> getAllAppInfo();

	public List<Application> getAllApp();

	public List<Application> findApp(int app_id);

	public List<Application> findAppByName(String name);

	public List<Application> sortAppByPrice(String sort);
	
	public List<Application> filterAppByPrice(int startFilter, int endFilter);

	public void saveRequest(int appId, String name, String organization, String platform, String version, String link,
			double price, String imgLink, String description);

	public List<Request> getRequests();

	public Request findRequest(int request_id);

	public void deleteRequest(int request_id);

	public void addApplication(int app_id, String name, String description, String organization, String imgLink,
			double price, String platform, String version, String link);

	public void deleteApplication(int app_id);

	public void deletePlatform(int app_id, String platform);

	public void updateApplication(int app_id, String name, String description, String organization, String imgLink,
			double price);
	
	public void updatePlatform(int app_id, String platform, String version, String link);
}
