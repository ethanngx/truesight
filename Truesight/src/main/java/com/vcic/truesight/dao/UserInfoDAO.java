package com.vcic.truesight.dao;

import com.vcic.truesight.model.UserInfo;

public interface UserInfoDAO {
	public UserInfo findUserInfo(String userName);
	public void addUserInfo(String username, String password);
}
