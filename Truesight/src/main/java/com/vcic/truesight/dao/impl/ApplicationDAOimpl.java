package com.vcic.truesight.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.vcic.truesight.dao.ApplicationDAO;
import com.vcic.truesight.mapper.ApplicationMapper;
import com.vcic.truesight.mapper.ApplicationPlatformsMapper;
import com.vcic.truesight.mapper.RequestMapper;
import com.vcic.truesight.model.Application;
import com.vcic.truesight.model.Request;

@Service
@Transactional
public class ApplicationDAOimpl extends JdbcDaoSupport implements ApplicationDAO{
	@Autowired
	public ApplicationDAOimpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Application> getAllAppInfo(){
		String sql = "SELECT a.App_ID,a.Name,a.Description,a.Organization,a.Price,a.Img,"
				+ "p.Platforms,p.Version,p.Link "
				+ " FROM Applications a "
				+ "INNER JOIN Applications_Platforms p ON a.App_ID = p.App_ID";
		ApplicationPlatformsMapper mapper = new ApplicationPlatformsMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Application> getAllApp(){
		String sql = "SELECT * FROM Applications ORDER BY app_id ASC";
		ApplicationMapper mapper = new ApplicationMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Application> findApp(int app_id) {
		String sql = "SELECT a.App_ID,a.Name,a.Description,a.Organization,a.Price,a.Img,"
				+ "p.Platforms,p.Version,p.Link "
				+ "FROM Applications a "
				+ "INNER JOIN Applications_Platforms p ON a.App_ID = p.App_ID "
				+ "WHERE a.App_ID = ?";
		Object[] params = new Object[] { app_id };
		ApplicationPlatformsMapper mapper = new ApplicationPlatformsMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, params, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Application> findAppByName(String name){
		String sql = "SELECT * FROM Applications a WHERE a.Name LIKE ? ";
		String finalName = "%" + StringUtils.capitalize(name.trim()) + "%";

		Object[] params = new Object[] { finalName };
		ApplicationMapper mapper = new ApplicationMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, params, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Application> sortAppByPrice(String sort){
		String sql;
		if (sort.equals("ascend")) {
			sql = "SELECT * FROM Applications ORDER BY price ASC ";
		} else {
			sql = "SELECT * FROM Applications ORDER BY price DESC ";
		}
		ApplicationMapper mapper = new ApplicationMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Application> filterAppByPrice(int startFilter, int endFilter){
		String sql = "SELECT * FROM Applications WHERE price >= ? AND price <= ? ";
		Object[] params = new Object[] { startFilter, endFilter };
		ApplicationMapper mapper = new ApplicationMapper();
		try {
			List<Application> appList = this.getJdbcTemplate().query(sql, params, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public void saveRequest(int reqId, String name, String organization, String platform, String version, String link,
			double price, String imgLink, String description) {
		String sql = "INSERT INTO Applications_Request "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		this.getJdbcTemplate().update(sql, reqId, name, description, organization, imgLink, platform, version, link, price);
	}
	
	@Override
	public List<Request> getRequests() {
		String sql = "SELECT * FROM Applications_Request ORDER BY request_id ASC";
		RequestMapper mapper = new RequestMapper();
		try {
			List<Request> appList = this.getJdbcTemplate().query(sql, mapper);
			return appList;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public Request findRequest(int request_id) {
		String sql = "SELECT * FROM Applications_Request WHERE request_id = ? ";
		Object[] params = new Object[] { request_id };
		RequestMapper mapper = new RequestMapper();
		try {
			Request request = this.getJdbcTemplate().queryForObject(sql, params, mapper);
			return request;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public void deleteRequest(int request_id) {
		String sql = "DELETE FROM Applications_Request WHERE request_id = ? ";
		this.getJdbcTemplate().update(sql, request_id);
	}
	
	@Override
	public void addApplication(int app_id, String name, String description, String organization, String imgLink,
			double price, String platform, String version, String link) {
		String sql1 = "INSERT INTO Applications "
				+ "VALUES (?, ?, ?, ?, ?, ?) ";
		String sql2 = "INSERT INTO Applications_Platforms "
				+ "VALUES (?, ?, ?, ?, ?) ";
		this.getJdbcTemplate().update(sql1, app_id, name, description, organization, imgLink, price);
		this.getJdbcTemplate().update(sql2, app_id, platform, version, link, price);
	}
	
	@Override
	public void deleteApplication(int app_id) {
		String sql1 = "DELETE FROM Applications_Platforms WHERE App_Id = ? ";
		String sql2 = "DELETE FROM Applications WHERE App_Id = ? ";
		this.getJdbcTemplate().update(sql1, app_id);
		this.getJdbcTemplate().update(sql2, app_id);
	}
	
	@Override
	public void deletePlatform(int app_id, String platform) {
		String sql = "DELETE FROM Applications_Platforms WHERE app_id = ? AND platforms = ? ";
		this.getJdbcTemplate().update(sql, app_id, platform);
	}
	
	@Override
	public void updateApplication(int app_id, String name, String description, String organization, String imgLink,
			double price) {
		String sql = "UPDATE Applications "
				+ "SET name = ?, description = ?, organization = ?, img = ?, price = ? "
				+ "WHERE app_id = ? ";
		this.getJdbcTemplate().update(sql, name, description, organization, imgLink, price, app_id);
	}
	
	@Override
	public void updatePlatform(int app_id, String platform, String version, String link) {
		String sql = "UPDATE Applications_Platforms "
				+ "SET version = ?, link = ? "
				+ "WHERE app_id = ? AND platforms = ? ";
		this.getJdbcTemplate().update(sql, version, link, app_id, platform);
	}
}
