package com.vcic.truesight.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vcic.truesight.dao.UserInfoDAO;
import com.vcic.truesight.mapper.UserInfoMapper;
import com.vcic.truesight.model.UserInfo;

@Service
@Transactional
public class UserInfoDAOimpl extends JdbcDaoSupport implements UserInfoDAO {

	@Autowired
	public UserInfoDAOimpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	@Override
	public UserInfo findUserInfo(String userName) {
		String sql = "SELECT u.Username,u.Password,u.User_Role "//
                + " FROM User_Account u WHERE u.Username = ? ";

		Object[] params = new Object[] { userName };
		UserInfoMapper mapper = new UserInfoMapper();
		try {
			UserInfo userInfo = this.getJdbcTemplate().queryForObject(sql, params, mapper);
			return userInfo;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public void addUserInfo(String username, String password) {
		String sql = "INSERT INTO User_Account "
				+ "VALUES (?, ?, 'USER', 1) ";
		this.getJdbcTemplate().update(sql, username, password);
	}
}
