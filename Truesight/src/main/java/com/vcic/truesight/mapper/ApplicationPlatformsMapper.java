package com.vcic.truesight.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.vcic.truesight.model.Application;

public class ApplicationPlatformsMapper implements RowMapper<Application> {
	@Override
	public Application mapRow(ResultSet rs, int rowNum) throws SQLException {
		int app_id = rs.getInt("App_Id");
		String name = rs.getString("Name");
		String description = rs.getString("Description");
		String organization = rs.getString("Organization");
		String platform = rs.getString("Platforms");
		String version = rs.getString("Version");
		String link = rs.getString("Link");
		double price = rs.getDouble("Price");
		String img = rs.getString("Img");
		
		return new Application(app_id, name, description, organization, platform, version, link, price, img);
	}
}
