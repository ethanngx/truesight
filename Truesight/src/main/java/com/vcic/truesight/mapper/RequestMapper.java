package com.vcic.truesight.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.vcic.truesight.model.Application;
import com.vcic.truesight.model.Request;

public class RequestMapper implements RowMapper<Request> {
	@Override
	public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
		int request_id = rs.getInt("Request_Id");
		String name = rs.getString("Name");
		String description = rs.getString("Description");
		String organization = rs.getString("Organization");
		String platform = rs.getString("Platforms");
		String version = rs.getString("Version");
		String link = rs.getString("Link");
		double price = rs.getDouble("Price");
		String img = rs.getString("Img");
		Application app = new Application(name, description, organization, platform, version, link, price, img);
		
		return new Request(request_id, app);
	}
}
