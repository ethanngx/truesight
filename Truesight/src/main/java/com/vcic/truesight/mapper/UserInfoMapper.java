package com.vcic.truesight.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.vcic.truesight.model.UserInfo;

public class UserInfoMapper implements RowMapper<UserInfo> {
	@Override
	public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		String userName = rs.getString("Username");
		String password = rs.getString("Password");
		String user_role = rs.getString("User_Role");

		return new UserInfo(userName, password, user_role);
	}
}
