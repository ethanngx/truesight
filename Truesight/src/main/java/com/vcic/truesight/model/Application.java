package com.vcic.truesight.model;

public class Application {
	private int app_id;
	private String name;
	private String description;
	private String organization;
	private String platform;
	private String version;
	private String link;
	private double price;
	private String img;
	
	public Application() {
		
	}
	
	public Application(int app_id, String name, String description, String organization, double price, String img) {
		this.app_id = app_id;
		this.name = name;
		this.description = description;
		this.organization = organization;
		this.price = price;
		this.img = img;
	}
	
	public Application(String name, String description, String organization, String platform,
			String version, String link, double price, String img) {
		this.name = name;
		this.description = description;
		this.organization = organization;
		this.platform = platform;
		this.version = version;
		this.link = link;
		this.price = price;
		this.img = img; 
	}
	
	public Application(int app_id, String name, String description, String organization, 
			String platform, String version, String link, double price, String img) {
		this.app_id = app_id;
		this.name = name;
		this.description = description;
		this.organization = organization;
		this.platform = platform;
		this.version = version;
		this.link = link;
		this.price = price;
		this.img = img;
	}

	public int getApp_id() {
		return app_id;
	}

	public void setApp_id(int app_id) {
		this.app_id = app_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "Application [app_id=" + app_id + ", name=" + name + ", description=" + description + ", organization="
				+ organization + ", platform=" + platform + ", version=" + version + ", link=" + link + ", price="
				+ price + ", img=" + img + "]";
	}
}
