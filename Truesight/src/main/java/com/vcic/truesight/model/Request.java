package com.vcic.truesight.model;

public class Request {
	private int request_id;
	private Application app;
	
	public Request() {
		
	}
	
	public Request(int request_id, Application app) {
		this.request_id = request_id;
		this.app = app;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public Application getApp() {
		return app;
	}

	public void setApp(Application app) {
		this.app = app;
	}	
}
