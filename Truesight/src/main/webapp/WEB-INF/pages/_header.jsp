<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="header">
	<nav class="navbar navbar-light shadow">
		<div class="container-fluid d-flex">

			<a class="navbar-brand fs-4 fw-bold me-5"
				href="${pageContext.request.contextPath}/">True Sight</a>
			<a class="navbar-brand flex-grow-1 fw-light"
				href="${pageContext.request.contextPath}/helpPage">About Us</a>
			<div>
				<form class="d-flex pe-5"
					action="${pageContext.request.contextPath}/search">
					<input class="form-control me-2" type="search" placeholder="Search"
						aria-label="Search" name="name">
					<button class="btn btn-outline-dark" type="submit">Search</button>
				</form>
			</div>

			<div class="d-flex">
				<c:if test="${pageContext.request.userPrincipal.name == null}">
					<form action="${pageContext.request.contextPath}/login">
						<button class="btn btn-lg btn-primary me-2" type="submit">Sign
							In</button>
					</form>
					<form action="${pageContext.request.contextPath}/signUp">
						<button class="btn btn-lg btn-primary" type="submit">Sign
							Up</button>
					</form>
				</c:if>

				<c:if test="${pageContext.request.userPrincipal.name != null}">
					<sec:authorize access="hasRole('ROLE_USER')">
						<form action="${pageContext.request.contextPath}/userInfo">
							<button class="btn btn-lg btn-dark me-2" type="submit">Account</button>
						</form>
					</sec:authorize>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<form action="${pageContext.request.contextPath}/admin">
							<button class="btn btn-lg btn-dark me-2" type="submit">Account</button>
						</form>
					</sec:authorize>
					<form action="${pageContext.request.contextPath}/logout">
						<button class="btn btn-lg btn-dark" type="submit">Log
							Out</button>
					</form>
				</c:if>
			</div>

		</div>
	</nav>
</div>