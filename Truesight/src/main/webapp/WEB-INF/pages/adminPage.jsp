<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<html>
<head>
<title>True Sight Admin</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5 shadow">
		<div>
			<h2 class="mb-5">Admin Page</h2>
			<h3>Welcome : ${pageContext.request.userPrincipal.name}</h3>
			<b>This is the exclusive admin page!</b>
		</div>
		<div class="mt-5">
			<h4>Admin's Options:</h4>
			<div class="accordion accordion-flush" id="adminFunctions">
				<div class="accordion-item">
					<h2 class="accordion-header" id="handleRequests">
						<button class="accordion-button collapsed" type="button"
							data-bs-toggle="collapse" data-bs-target="#handleRequestsBody"
							aria-expanded="false" aria-controls="handleRequestsBody">Users'
							Requests</button>
					</h2>
					<div id="handleRequestsBody" class="accordion-collapse collapse"
						aria-labelledby="handleRequests" data-bs-parent="#adminFunctions">
						<div class="accordion-body">
							<c:if test="${reqList.size() == 0}">
								<p class="fst-italic">No pending request.</p>
							</c:if>
							<c:if test="${reqList.size() != 0}">
								<table class="table">
									<thead>
										<tr>
											<th scope="col">Request ID</th>
											<th scope="col">Application Name</th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${reqList}" var="req">
											<tr>
												<td>${req.request_id}</td>
												<td>${req.app.name}</td>
												<td><a
													href="${pageContext.request.contextPath}/request/${req.request_id}"
													class="btn btn-primary">More details</a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="handleApps">
						<button class="accordion-button collapsed" type="button"
							data-bs-toggle="collapse" data-bs-target="#handleAppsBody"
							aria-expanded="false" aria-controls="handleAppsBody">Application
							Repository</button>
					</h2>
					<div id="handleAppsBody" class="accordion-collapse collapse"
						aria-labelledby="handleApps" data-bs-parent="#adminFunctions">
						<div class="accordion-body">
							<a href="${pageContext.request.contextPath}/addApp"
								class="btn btn-primary">Add An Application</a>
							<table class="table">
								<thead>
									<tr>
										<th scope="col">Application ID</th>
										<th scope="col">Application Name</th>
										<th scope="col">Organization</th>
										<th scope="col">Price</th>
										<th scope="col"></th>
										<th scope="col"></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${appList}" var="app">
										<tr>
											<td>${app.app_id}</td>
											<td>${app.name}</td>
											<td>${app.organization}</td>
											<td>${app.price}</td>
											<td><a
												href="${pageContext.request.contextPath}/appUpdate/${app.app_id}"
												class="btn btn-primary">Update</a></td>
											<td><a
												href="${pageContext.request.contextPath}/deleteApp/${app.app_id}"
												class="btn btn-primary">Delete</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>