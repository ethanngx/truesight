<html>
<head>
<title>True Sight</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5 shadow">
		<h1 class="mb-5">About Us</h1>
		<h5>What is TrueSight?</h5>
		<p>TrueSight is a website-based application that provides more
			information, is user-driven, and allows cross-platform comparison. It
			is designed for you to easily compare applications and choose your
			perfect one. Also, with registration, you can suggest adding new
			useful applications.</p>
		<br>
		<h5>New to TrueSight?</h5>
		<p>Here, you can view, sort, and filter all the applications in
			the repository without logging in. You can also search for what you
			want on every page. You will get the application information
			including the names, organizations, platforms, versions of it that
			can be installed, an external link to the respective stores, and the
			price. You can sort and filter application information according to
			the price of the application. You are welcome to join TrueSight by
			creating an account. All registered users have the ability to submit
			the new application request form. Join and help us to make TrueSight
			better!</p>
		<br>
		<h5>How to register?</h5>
		<ul>
			<li>Click on the Signup Button</li>
			<li>Simply register by creating the username and setting the
				password you like</li>
			<li>Now you are the registered user of TrueSight!</li>
		</ul>
		<br>
		<h5>How to add a new application to the repository?</h5>
		<ul>
			<li>If you want to submit a request, you need to log in to your
			account</li>
			<li>In order to add a new application to the repository, you
			need to submit a request form first with required information</li>
			<li>Our admin will verify if it is valid and your application is
			added to TrueSight!</li>
		</ul>
	</div>
</body>
</html>