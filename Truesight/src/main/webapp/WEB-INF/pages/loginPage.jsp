<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Login</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/style.css" />" />
</head>
<body>
	<jsp:include page="_header.jsp" />

	<div
		class="container h-100 d-flex align-items-center justify-content-center">
		<div
			class="d-flex flex-column justify-content-center align-items-center rounded h-70 p-5 bg-primary text-light">

			<h1 class="my-5">
				<b>Login</b>
			</h1>


			<form
				class="d-flex flex-column justify-content-center align-items-center d-grid gap-3 py-3 px-5"
				method="POST"
				action="${pageContext.request.contextPath}/j_spring_security_check">
				<div>
					<label for="username">Username</label> <input type="text"
						class="form-control" name="username" placeholder="Username" required>
				</div>
				<div>
					<label for="password">Password</label> <input type="password"
						class="form-control" name="password" placeholder="Password" required>
				</div>
				
				<!-- /login?error=true -->
				<c:if test="${param.error == 'true'}">
					<div class="error-message">
						Login Failed<br />
						${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
					</div>
				</c:if>

				<button class="btn btn-light" type="submit">Log In</button>
			</form>
		</div>
	</div>
</body>
</html>