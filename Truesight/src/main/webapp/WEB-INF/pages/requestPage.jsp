<html>
<head>
<title>True Sight Admin</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5 shadow">
		<h3 class="mb-5">Request Form From User</h3>
		<div>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td>Request ID</td>
						<td>${req.request_id}</td>
					</tr>
					<tr>
						<td>Application Name</td>
						<td>${req.app.name}</td>
					</tr>
					<tr>
						<td>Application Description</td>
						<td>${req.app.description}</td>
					</tr>
					<tr>
						<td>Application Organization</td>
						<td>${req.app.organization}</td>
					</tr>
					<tr>
						<td>Application Platform</td>
						<td>${req.app.platform}</td>
					</tr>
					<tr>
						<td>Application Version</td>
						<td>${req.app.version}</td>
					</tr>
					<tr>
						<td>Application Link</td>
						<td>${req.app.link}</td>
					</tr>
					<tr>
						<td>Application Price</td>
						<td>${req.app.price}</td>
					</tr>
					<tr>
						<td>Application Image Link</td>
						<td>${req.app.img}</td>
					</tr>
				</tbody>
			</table>
			<form action="${pageContext.request.contextPath}/acceptRequest">
				<input type="hidden" name="rejectId" value="${req.request_id}"/>
				<input type="hidden" name="name" value="${req.app.name}"/>
				<input type="hidden" name="description" value="${req.app.description}"/>
				<input type="hidden" name="organization" value="${req.app.organization}"/>
				<input type="hidden" name="platform" value="${req.app.platform}"/>
				<input type="hidden" name="version" value="${req.app.version}"/>
				<input type="hidden" name="link" value="${req.app.link}"/>
				<input type="hidden" name="price" value="${req.app.price}"/>
				<input type="hidden" name="img" value="${req.app.img}"/>
				<button class="btn btn-primary" type="submit">Accept</button>
			</form>
			<form action="${pageContext.request.contextPath}/rejectRequest">
				<input type="hidden" name="rejectId" value="${req.request_id}"/>
				<button class="btn btn-primary" type="submit">Reject</button>
			</form>
		</div>
	</div>
</body>
</html>