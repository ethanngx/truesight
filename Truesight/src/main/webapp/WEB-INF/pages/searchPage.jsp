<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${search} - Search</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container py-3">
		<div class="p-4">
			<h2>Search Result for "${search}"</h2>
		</div>
		<div>
			<c:forEach items="${appList}" var="app">
				<div class="card mb-5 p-4 border-light shadow">
					<div class="row g-0">
						<div class="col-md-3">
							<img src="${app.img}" alt="...">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">${app.name}</h5>
								<p class="card-text">${app.description}</p>
								<a href="${pageContext.request.contextPath}/${app.app_id}"
									class="btn btn-primary">More details</a>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>