<%@page session="false"%>
<html>
<head>
<title>True Sight</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />

	<div class="container p-5 shadow">
		<div>
			<h2 class="mb-5">User Page</h2>
			<h3>Hello ${pageContext.request.userPrincipal.name},</h3>
			<b>Welcome to True Sight!</b>
		</div>
		<div class="mt-5">
			<h4>User's Options:</h4>
			<div class="accordion accordion-flush" id="userFunctions">
				<div class="accordion-item">
					<h2 class="accordion-header" id="requestForm">
						<button class="accordion-button collapsed" type="button"
							data-bs-toggle="collapse" data-bs-target="#reqForm"
							aria-expanded="false" aria-controls="reqForm">Add An
							Application Request</button>
					</h2>
					<div id="reqForm" class="accordion-collapse collapse"
						aria-labelledby="requestForm" data-bs-parent="#userFunctions">
						<div class="accordion-body">
							<form class="row g-3" action="${pageContext.request.contextPath}/requested">
								<div class="col-md-4">
									<label for="appName" class="form-label">Application
										Name</label> <input type="text" name="appName" class="form-control" id="appName"
										required>
								</div>
								<div class="col-md-4">
									<label for="organization" class="form-label">Organization</label>
									<input type="text" name="organization" class="form-control" id="organization"
										required>
								</div>
								<div class="col-md-4">
									<label for="platform" class="form-label">Platform</label> <input
										type="text" name="platform" class="form-control" id="platform" required>
								</div>
								<div class="col-md-3">
									<label for="version" class="form-label">Version</label> <input
										type="text" name="version" class="form-control" id="version" required>
								</div>
								<div class="col-md-6">
									<label for="link" class="form-label">Store Link</label> <input
										type="url" name="link" class="form-control" id="link" required>
								</div>
								<div class="col-md-3">
									<label for="price" class="form-label">Price</label> <input
										type="number" step="0.01" name="price" class="form-control" id="price" required>
								</div>
								<div class="col-md-12">
									<label for="imgLink" class="form-label">Application
										Logo Image Link</label> <input type="url" name="imgLink" class="form-control"
										id="imgLink" required>
								</div>
								<div class="col-md-12">
									<label for="description" class="form-label">Description</label>
									<input type="text" name="description" class="form-control" id="description"
										required>
								</div>
								<div class="col-12">
									<button class="btn btn-primary" type="submit">Submit
										form</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>