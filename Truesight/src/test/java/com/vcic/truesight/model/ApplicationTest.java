package com.vcic.truesight.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ApplicationTest {

	private Application app;
	private final static double EPSILON = 0.00001;

	@Test
	public void testApplicationStringStringStringStringStringStringDouble() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		assertEquals(app.getApp_id(), 1);
		assertEquals(app.getName(), "demo");
		assertEquals(app.getDescription(), "description");
		assertEquals(app.getOrganization(), "microsoft");
		assertEquals(app.getPlatform(), "ios");
		assertEquals(app.getVersion(), "1.5");
		assertEquals(app.getLink(), "demo link");
		assertTrue(app.getPrice() - 0.99 < EPSILON);
		assertEquals(app.getImg(), "img");
	}

	@Test
	public void testSetApp_id() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setApp_id(2);
		assertEquals(app.getApp_id(), 2);
	}
	
	@Test
	public void testSetName() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setName("demoSet");
		assertEquals(app.getName(), "demoSet");
	}

	@Test
	public void testSetDescription() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setDescription("setDescription");
		assertEquals(app.getDescription(), "setDescription");
	}

	@Test
	public void testSetOrganization() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setOrganization("apple");
		assertEquals(app.getOrganization(), "apple");
	}

	@Test
	public void testSetPlatforms() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setPlatform("android");
		assertEquals(app.getPlatform(), "android");
	}

	@Test
	public void testSetVersion() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setVersion("1.6");
		assertEquals(app.getVersion(), "1.6");
	}

	@Test
	public void testSetLink() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setLink("set demo link");
		assertEquals(app.getLink(), "set demo link");
	}

	@Test
	public void testSetPrice() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setPrice(1.99);
		assertTrue(app.getPrice() - 1.99 < EPSILON);
	}
	
	@Test
	public void testSetImg() {
		app = new Application(1, "demo", "description", "microsoft", "ios", "1.5", "demo link", 0.99, "img");
		app.setImg("new img");
		assertEquals(app.getImg(), "new img");
	}
}
