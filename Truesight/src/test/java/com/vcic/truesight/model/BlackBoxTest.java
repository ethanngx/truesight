package com.vcic.truesight.model;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.Principal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.ui.Model;

import com.vcic.truesight.controller.MainController;
import com.vcic.truesight.model.UserInfo;

public class BlackBoxTest {

	class TestModel implements Model {
		Map<String, Object> attributes = new HashMap<>();

		@Override
		public Model mergeAttributes(Map<String, ?> arg0) {
			return null;
		}

		@Override
		public boolean containsAttribute(String arg0) {
			return false;
		}

		@Override
		public Map<String, Object> asMap() {
			return attributes;
		}

		@Override
		public Model addAttribute(String arg0, Object arg1) {
			attributes.put(arg0, arg1);
			return this;
		}

		@Override
		public Model addAttribute(Object arg0) {
			return null;
		}

		@Override
		public Model addAllAttributes(Map<String, ?> arg0) {
			return null;
		}

		@Override
		public Model addAllAttributes(Collection<?> arg0) {
			return null;
		}
	}

	// four tests

	// ensure the UserInfo class correctly stores the parameter fields and is
	// able to correctly reiterate the values
	// to the user of the class
	@Test
	public void testUserInfoType() {
		String userName = "myusername";
		String password = "mypassword";
		String userRole = "clientuser";
		UserInfo info = new UserInfo(userName, password, userRole);
		assertTrue(String.format("UserInfo [userName=%s, password=%s, user_role=%s]", userName, password, userRole)
				.equals(info.toString()));
	}

	// Ensure that calling the method for denied access with a specific
	// Principal object
	// Will yield appropriate message to the user in the model map
	@Test
	public void testMainControllerAccessDenied() {
		MainController mc = new MainController();
		Model m = new TestModel();
		mc.accessDenied(m, new Principal() {

			@Override
			public String getName() {
				return "testuser";
			}
		});

		assertTrue(m.asMap().get("message")
				.equals(String.format("Hi %s<br> You do not have permission to access this page!", "testuser")));

	}

	// Ensure that signUp web request leads to signUpPage
	@Test
	public void testSignUpPage() {
		MainController mc = new MainController();

		assertEquals("signUpPage", mc.signupPage(null));

	}

	// Ensure that userInfo web request leads to userInfoPage
	@Test
	public void testUserInfo() {
		MainController mc = new MainController();
		assertEquals("userInfoPage", mc.userInfo(null, null));
	}
}
