package com.vcic.truesight.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import com.vcic.truesight.mapper.ApplicationMapper;
import com.vcic.truesight.mapper.RequestMapper;
import com.vcic.truesight.mapper.UserInfoMapper;
import com.vcic.truesight.model.Application;
import com.vcic.truesight.model.Request;
import com.vcic.truesight.model.UserInfo;

public class GreyBoxTest {

	class UserInfoResultSet extends AbstractResultSet {
		@Override
		public String getString(String columnLabel) throws SQLException {
			switch (columnLabel) {
			case "Username":
				return "testuser";
			case "Password":
				return "12345";
			case "User_Role":
				return "clientuser";
			}
			return null;
		}
	}

	class ApplicationResultSet extends AbstractResultSet {

		@Override
		public int getInt(String columnLabel) throws SQLException {
			if (columnLabel.equals("App_Id")) {
				return 1;
			}
			return -1;
		}

		@Override
		public double getDouble(String columnLabel) throws SQLException {
			if (columnLabel.equals("Price")) {
				return 100;
			}
			return -1;
		}

		@Override
		public String getString(String columnLabel) throws SQLException {

			switch (columnLabel) {
			case "Name":
				return "demo";
			case "Description":
				return "description";
			case "Organization":
				return "microsoft";
			case "Img":
				return "img";
			}
			return null;
		}
	}

	class RequestResultSet extends AbstractResultSet {
		@Override
		public int getInt(String columnLabel) throws SQLException {
			if (columnLabel.equals("Request_Id")) {
				return 111;
			}
			return -1;
		}

		@Override
		public String getString(String columnLabel) throws SQLException {
			return null;
		}

		@Override
		public double getDouble(String columnLabel) throws SQLException {
			return -1;
		}
	}

	// Testing the mappers is considered grey box because
	// the mappers are more detailed than what the end user needs to know or
	// cares to know
	// but the testers need to ensure that
	// result sets are correctly mapped to objects which are correctly populated

	@Test
	public void testUserInfoMapper() {
		UserInfoMapper m = new UserInfoMapper();
		ResultSet rs = new UserInfoResultSet();
		try {
			UserInfo ui = m.mapRow(rs, 0);
			assertEquals("testuser", ui.getUserName());
			assertEquals("12345", ui.getPassword());
			assertEquals("clientuser", ui.getUser_role());

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testApplicationMapper() {
		ApplicationMapper m = new ApplicationMapper();
		ResultSet rs = new ApplicationResultSet();
		try {
			Application app = m.mapRow(rs, 0);
			assertEquals(String.format(
					"Application [app_id=%s, name=%s, description=%s, organization=%s, platform=%s, version=%s, link=%s, price=%.1f, img=%s]",
					"1", "demo", "description", "microsoft", "null", "null", "null", 100.0, "img"), app.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRequestMapper() {
		RequestMapper m = new RequestMapper();
		ResultSet rs = new RequestResultSet();
		try {
			Request r = m.mapRow(rs, 0);
			assertEquals(111, r.getRequest_id());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
