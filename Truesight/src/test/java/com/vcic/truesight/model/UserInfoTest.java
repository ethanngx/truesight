package com.vcic.truesight.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserInfoTest {

	private UserInfo user;
	
	@Test
	public void testUserInfo() {
		user = new UserInfo();
		assertEquals(user.getUserName(), null);
		assertEquals(user.getPassword(), null);
		assertEquals(user.getUser_role(), null);
	}

	@Test
	public void testUserInfoStringStringString() {
		user = new UserInfo("user1", "goodpassword123", "USER");
		assertEquals(user.getUserName(), "user1");
		assertEquals(user.getPassword(), "goodpassword123");
		assertEquals(user.getUser_role(), "USER");
	}

	@Test
	public void testGetUserName() {
		user = new UserInfo("user1", "goodpassword123", "USER");
		assertEquals(user.getUserName(), "user1");
	}

	@Test
	public void testSetUserName() {
		user = new UserInfo();
		user.setUserName("anth12");
		assertEquals(user.getUserName(), "anth12");
	}

	@Test
	public void testGetPassword() {
		user = new UserInfo("user1", "goodpassword123", "USER");
		assertEquals(user.getPassword(), "goodpassword123");
	}

	@Test
	public void testSetPassword() {
		user = new UserInfo();
		user.setPassword("helloPass");
		assertEquals(user.getPassword(), "helloPass");
	}

	@Test
	public void testGetUser_role() {
		user = new UserInfo("user1", "goodpassword123", "USER");
		assertEquals(user.getUser_role(), "USER");
	}

	@Test
	public void testSetUser_role() {
		user = new UserInfo();
		user.setUser_role("ADMIN");
		assertEquals(user.getUser_role(), "ADMIN");
	}

	@Test
	public void testToString() {
		user = new UserInfo("user1", "goodpassword123", "USER");
		assertEquals(user.toString(), "UserInfo [userName=user1, password=goodpassword123, user_role=USER]");
	}

}
