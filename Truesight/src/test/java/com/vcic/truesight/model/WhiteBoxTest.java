package com.vcic.truesight.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.vcic.truesight.config.SpringWebInitializer;
import com.vcic.truesight.config.WebMvcConfig;
import com.vcic.truesight.config.WebSecurityConfig;

public class WhiteBoxTest {
	// reflection can be used as everything including the implementation is
	// transparent

	@Test
	public void testWebMVCConfigMethods() {
		assertEquals(3, WebMvcConfig.class.getDeclaredMethods().length);
	}

	@Test
	public void testWebSecurityConfigFields() {
		assertEquals(1, WebSecurityConfig.class.getDeclaredFields().length);
		assertEquals("myDBAuthenticationService", WebSecurityConfig.class.getDeclaredFields()[0].getName());
	}

	@Test
	public void testSpringWebInitializerMethods() {
		assertEquals(1, SpringWebInitializer.class.getDeclaredMethods().length);
		assertEquals("onStartup", SpringWebInitializer.class.getDeclaredMethods()[0].getName());

	}
}
