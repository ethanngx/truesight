<%@page session="false"%>
<html>
<head>
<title>Access Denied</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5">
		<h3 style="color: red;">${message}</h3>
	</div>
</body>
</html>