<html>
<head>
<title>Accept Request</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container shadow p-5">
		<h1>Add An Application</h1>
		<form class="row g-3"
			action="${pageContext.request.contextPath}/addSuccess">
			<div class="col-md-4">
				<label for="name" class="form-label">Application Name</label> <input
					type="text" name="name" class="form-control" id="name"
					required>
			</div>
			<div class="col-md-4">
				<label for="organization" class="form-label">Organization</label> <input
					type="text" name="organization" class="form-control"
					id="organization" required>
			</div>
			<div class="col-md-4">
				<label for="platform" class="form-label">Platform</label> <input
					type="text" name="platform" class="form-control" id="platform"
					required>
			</div>
			<div class="col-md-3">
				<label for="version" class="form-label">Version</label> <input
					type="text" name="version" class="form-control" id="version"
					required>
			</div>
			<div class="col-md-6">
				<label for="link" class="form-label">Store Link</label> <input
					type="url" name="link" class="form-control" id="link" required>
			</div>
			<div class="col-md-3">
				<label for="price" class="form-label">Price</label> <input
					type="number" step="0.01" name="price" class="form-control"
					id="price" required>
			</div>
			<div class="col-md-12">
				<label for="img" class="form-label">Application Logo
					Image Link</label> <input type="url" name="img" class="form-control"
					id="img" required>
			</div>
			<div class="col-md-12">
				<label for="description" class="form-label">Description</label> <input
					type="text" name="description" class="form-control"
					id="description" required>
			</div>
			<div class="col-12">
				<button class="btn btn-primary" type="submit">Add</button>
			</div>
		</form>
	</div>
</body>
</html>