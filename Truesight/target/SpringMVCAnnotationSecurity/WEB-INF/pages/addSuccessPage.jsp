<html>
<head>
<title>True Sight</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5">
		<h1>Success!</h1>
		<h3>Application has been added to the repository</h3>
	</div>
</body>
</html>