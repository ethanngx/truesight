<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${app.name}</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container p-5 shadow">
		<div class="mb-4">
			<img src="${app.img}" alt="...">
			<h1>${app.name}</h1>
			<h6>${app.organization}</h6>
			<c:choose>
				<c:when test="${app.price == 0}">
					<p class="card-text">Free</p>
				</c:when>
				<c:otherwise>
					<p class="card-text">$${app.price}</p>
				</c:otherwise>
			</c:choose>
		</div>
		<p class="my-5">${app.description}</p>
		<c:forEach items="${list}" var="p">
			<div class="my-3">
				<h5>Platform: ${p.platform}</h5>
				<p>Version: ${p.version}</p>
				<a href="${p.link}"><button type="button" class="btn btn-primary">Go to store</button></a>
			</div>
		</c:forEach>
	</div>
</body>
</html>