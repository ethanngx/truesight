<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Accept Request</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container shadow p-5">
		<h1 class="mb-5">Update ${app.name}</h1>
		<form action="${pageContext.request.contextPath}/updateSuccess">
			<h3>General Information</h3>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td>Application Name</td>
						<td><input type="text" name="name" value="${app.name}"
							required /></td>
					</tr>
					<tr>
						<td>Application Description</td>
						<td><input type="text" name="description"
							value="${app.description}" required /></td>
					</tr>
					<tr>
						<td>Application Organization</td>
						<td><input type="text" name="organization"
							value="${app.organization}" required /></td>
					</tr>
					<tr>
						<td>Application Image Link</td>
						<td><input type="url" name="img" value="${app.img}" required /></td>
					</tr>
					<tr>
						<td>Application Price</td>
						<td><input type="number" step="0.01" name="price"
							value="${app.price}" required /></td>
					</tr>
				</tbody>
			</table>
			<h3>Different Platforms</h3>
			<p class="fst-italic">*Platform cannot be updated. Either delete
				it or update the version and link to store.</p>
			<c:forEach items="${list}" var="p">
				<input type="hidden" name="platform" value="${p.platform}"/>
				<table class="table table-bordered mb-4">
					<tbody>
						<tr>
							<td>Application Platform</td>
							<td>${p.platform}</td>
							<td><a
								href="${pageContext.request.contextPath}/deletePlatform/${app.app_id}/${p.platform}"
								class="btn btn-primary">Delete</a></td>
						</tr>	
						<tr>
							<td>Application Version</td>
							<td><input type="text" name="version" value="${p.version}"
								required /></td>
						</tr>
						<tr>
							<td>Application Link</td>
							<td><input type="url" name="link" value="${p.link}" required /></td>
						</tr>
					</tbody>
				</table>
			</c:forEach>
			<input type="hidden" name="app_id" value="${app_id}" />
			<button class="btn btn-primary" type="submit">Update</button>
		</form>
	</div>
</body>
</html>