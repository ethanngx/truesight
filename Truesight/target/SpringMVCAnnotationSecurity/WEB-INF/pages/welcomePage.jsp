<%@page session="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>True Sight</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="_header.jsp" />
	<div class="container py-3">
		<div class="p-4">
			<h1>Welcome to True Sight</h1>
			<p>Find Your Perfect Application</p>
		</div>
		<div class="shadow">
			<div class="d-flex">
				<h3 class="align-self-center flex-grow-1 ps-5">App List</h3>
				<!-- Filter & Sort -->
				<div class="accordion accordion-flush d-flex" id="filterSort">
					<!-- Sort -->
					<div class="accordion-item">
						<h2 class="accordion-header" id="sortHeading">
							<button class="accordion-button collapsed" type="button"
								data-bs-toggle="collapse" data-bs-target="#sort"
								aria-expanded="false" aria-controls="sort">Sort</button>
						</h2>
						<div id="sort" class="accordion-collapse collapse"
							aria-labelledby="sortHeading" data-bs-parent="#filterSort">
							<div class="accordion-body">
								<form action="${pageContext.request.contextPath}/sort">
									<input type="radio" name="sortPrice" id="ascend" value="ascend">
									<label for="ascend"> Ascending Price </label> <input
										type="radio" name="sortPrice" id="descend" value="descend">
									<label for="descend"> Descending Price </label>
									<button type="submit">Sort</button>
								</form>
							</div>
						</div>
					</div>

					<!-- Filter -->
					<div class="accordion-item">
						<h2 class="accordion-header" id="filterHeading">
							<button class="accordion-button collapsed" type="button"
								data-bs-toggle="collapse" data-bs-target="#filter"
								aria-expanded="false" aria-controls="filter">Filter</button>
						</h2>
						<div id="filter" class="accordion-collapse collapse"
							aria-labelledby="filterHeading" data-bs-parent="#filterSort">
							<div class="accordion-body">
								<form action="${pageContext.request.contextPath}/filter">
									<label for="filterPrice" class="form-label">Enter Price
										Range</label> <input type="number" name="startFilterPrice" step="0.01"
										id="filterPrice" required /> - <input type="number" name="endFilterPrice" step="0.01" required />
									<button type="submit">Filter</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- App List -->
			<c:forEach items="${appList}" var="app">
				<div class="card mb-5 p-4 border-light shadow">
					<div class="row g-0">
						<div class="col-md-3">
							<img src="${app.img}" alt="...">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">${app.name}</h5>
								<c:choose>
									<c:when test="${app.price == 0}">
										<p class="card-text">Free</p>
									</c:when>
									<c:otherwise>
										<p class="card-text">$${app.price}</p>
									</c:otherwise>
								</c:choose>
								<p class="card-text">${app.description}</p>
								<a href="${pageContext.request.contextPath}/app/${app.app_id}"
									class="btn btn-primary">More details</a>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>